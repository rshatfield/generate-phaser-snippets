(ns phaserSnippets.core
  (:require [clojure.string :as string]
            [cljs.nodejs :as nodejs]))

(nodejs/enable-util-print!)
(def jdata (nodejs/require "./data.json"))
(def fs (nodejs/require "fs"))
(def cdata (js->clj jdata))

(defn -main [& args]

  (doall (map 
    (fn [obj]
      (let [scope (obj "scope")
            memberof (obj "memberof")
            nm (obj "name")
            description (obj "description")
            classdesc (obj "classdesc")
            returns (or classdesc (get-in obj ["properties" 0 "type" "names" 0] "void"))
            kind (obj "kind")
            params-list (obj "params")
            params  (if params-list
                      (string/join ", "
                        (map 
                          (fn [p]
                            (let [_type (get-in p ["type" "names" 0])
                                 _default (p "defaultvalue")
                                 _optional (p "optional")
                                 nm (p "name")
                                 nm (if _default (str nm "=" _default) nm)
                                 nm (if _optional 
                                      (str "[" nm " (" _type ")]") 
                                      (str nm " (" _type ")"))]
                              nm))
                          params-list))
                      "")
            file-nm (str "out/" memberof "." nm ".sublime-snippet")
            snippet-scope "source.js.phaser,source.coffee.phaser"
            snippet-description (str "(" returns ") " memberof "." nm "(" params ")")
            set-var (if (and (not= returns "void") (or (not= scope "static") (= kind "class")))
                      (string/replace (string/lower-case returns) "." "_")
                      nil)

            ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            ;; TODO:  
            ;;  1. test and correct set-var ex. number = alpha should be number = Phaser.SpriteBatch.alpha
            ;;  2. if we have set-var, make that ${1:}, instead of the first argument
            ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

            snippet-content 
              (str 
                (or (and set-var (str set-var " = ")) "") ; set var = obj.fn() or obj.attribute

                (if (and (= kind "class") (> (count classdesc) 0))
                  "new " ;constructor call
                  "") 

                (if (= scope "static")
                  (str memberof "." nm )
                  nm)

                (if (not= kind "member") ;function call
                  (str 
                    "(" (string/join ""
                      (map-indexed 
                        (fn [idx itm]
                          (let [nm (itm "name")
                                optional (itm "optional")
                                param-num (inc idx)]
                            (str (if (and (not optional) (> idx 0)) ", " "") "${" param-num ":" (if (and optional (> idx 0)) ", " "") nm "}")))
                       params-list)) ")"))
                )
            snippet-tagtrigger (str (string/replace memberof "." "-") "-" nm)
            snippet 
              (str "<snippet>\n"
                   "<content><![CDATA[" snippet-content "]]></content>\n"
                   "<tabTrigger>" snippet-tagtrigger "</tabTrigger>\n"
                   "<scope>" snippet-scope "</scope>\n"
                   "<description>" (string/replace (string/replace snippet-description "<" "") ">" "") "</description>\n"
                   "</snippet>\n")
          ]

          (when true
            (.writeFileSync fs file-nm snippet)
            (println file-nm))


          ))
    (sort-by 
      #(string/lower-case (str (%1 "memberof") (%1 "kind") (%1 "name")))
      (filter 
        #(and 
          (not= (first (%1 "name")) "_")
          (not (%1 "undocumented"))
          (and (> (count (%1 "memberof")) 0) (< (.indexOf (%1 "memberof") "anonymous") 0))
          (> (count (%1 "kind")) 0)
          (> (count (%1 "name")) 0))
        cdata)))))

(set! *main-cli-fn* -main)