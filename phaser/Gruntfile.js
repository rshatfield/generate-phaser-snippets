module.exports = function (grunt) {
    //npm install grunt --save-dev
    //npm install grunt-cli --save-dev
    //npm install jsdoc --save-dev
    //npm install grunt-jsdoc --save-dev
    
    grunt.initConfig({
        jsdoc : {
          dist : 
          { src: [
            'src/Phaser.js',
            'src/core/*.js',
            'src/gameobjects/*.js',
            'src/gameobjects/*/*.js',
            'src/geom/*.js',
            'src/input/*.js',
            'src/loader/*.js',
            'src/math/*.js',
            'src/net/*.js',
            'src/particles/*.js',
            'src/particles/*/*.js',
            'src/physics/*.js',
            'src/physics/arcade/*.js',
            'src/sound/*.js',
            'src/system/*.js',
            'src/tilemap/*.js',
            'src/time/*.js',
            'src/tween/*.js',
            'src/utils/*.js'],
            options: {
                destination: 'phaser-jsdoc',
                explain: true
            }
          }}});

    // grunt.initConfig({
    //     jsdoc : {
    //       dist : 
    //       { src: [
    //          'src/pixi/Pixi.js',
    //          'src/pixi/display/DisplayObject.js',
    //          'src/pixi/display/DisplayObjectContainer.js',
    //          'src/pixi/display/Sprite.js'
    //          ],
    //         options: {
    //             destination: 'pixi-jsdoc'
    //         }
    //       }}});

    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.registerTask('default', ['jsdoc']);
};
