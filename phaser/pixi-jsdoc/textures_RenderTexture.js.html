<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JSDoc: Source: textures/RenderTexture.js</title>

    <script src="scripts/prettify/prettify.js"> </script>
    <script src="scripts/prettify/lang-css.js"> </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="styles/prettify-tomorrow.css">
    <link type="text/css" rel="stylesheet" href="styles/jsdoc-default.css">
</head>

<body>

<div id="main">

    <h1 class="page-title">Source: textures/RenderTexture.js</h1>

    



    
    <section>
        <article>
            <pre class="prettyprint source linenums"><code>/**
 * @author Mat Groves http://matgroves.com/ @Doormat23
 */

/**
 * A RenderTexture is a special texture that allows any Pixi display object to be rendered to it.
 *
 * __Hint__: All DisplayObjects (i.e. Sprites) that render to a RenderTexture should be preloaded otherwise black rectangles will be drawn instead.
 *
 * A RenderTexture takes a snapshot of any Display Object given to its render method. The position and rotation of the given Display Objects is ignored. For example:
 *
 *    var renderTexture = new PIXI.RenderTexture(800, 600);
 *    var sprite = PIXI.Sprite.fromImage("spinObj_01.png");
 *    sprite.position.x = 800/2;
 *    sprite.position.y = 600/2;
 *    sprite.anchor.x = 0.5;
 *    sprite.anchor.y = 0.5;
 *    renderTexture.render(sprite);
 *
 * The Sprite in this case will be rendered to a position of 0,0. To render this sprite at its actual position a DisplayObjectContainer should be used:
 *
 *    var doc = new PIXI.DisplayObjectContainer();
 *    doc.addChild(sprite);
 *    renderTexture.render(doc);  // Renders to center of renderTexture
 *
 * @class RenderTexture
 * @extends Texture
 * @constructor
 * @param width {Number} The width of the render texture
 * @param height {Number} The height of the render texture
 * @param renderer {CanvasRenderer|WebGLRenderer} The renderer used for this RenderTexture
 * @param scaleMode {Number} See {{#crossLink "PIXI/scaleModes:property"}}PIXI.scaleModes{{/crossLink}} for possible values
 * @param resolution {Number} The resolution of the texture being generated
 */
PIXI.RenderTexture = function(width, height, renderer, scaleMode, resolution)
{
    /**
     * The with of the render texture
     *
     * @property width
     * @type Number
     */
    this.width = width || 100;

    /**
     * The height of the render texture
     *
     * @property height
     * @type Number
     */
    this.height = height || 100;

    /**
     * The Resolution of the texture.
     *
     * @property resolution
     * @type Number
     */
    this.resolution = resolution || 1;

    /**
     * The framing rectangle of the render texture
     *
     * @property frame
     * @type Rectangle
     */
    this.frame = new PIXI.Rectangle(0, 0, this.width * this.resolution, this.height * this.resolution);

    /**
     * This is the area of the BaseTexture image to actually copy to the Canvas / WebGL when rendering,
     * irrespective of the actual frame size or placement (which can be influenced by trimmed texture atlases)
     *
     * @property crop
     * @type Rectangle
     */
    this.crop = new PIXI.Rectangle(0, 0, this.width * this.resolution, this.height * this.resolution);

    /**
     * The base texture object that this texture uses
     *
     * @property baseTexture
     * @type BaseTexture
     */
    this.baseTexture = new PIXI.BaseTexture();
    this.baseTexture.width = this.width * this.resolution;
    this.baseTexture.height = this.height * this.resolution;
    this.baseTexture._glTextures = [];
    this.baseTexture.resolution = this.resolution;

    this.baseTexture.scaleMode = scaleMode || PIXI.scaleModes.DEFAULT;

    this.baseTexture.hasLoaded = true;

    PIXI.Texture.call(this,
        this.baseTexture,
        new PIXI.Rectangle(0, 0, this.width * this.resolution, this.height * this.resolution)
    );

    /**
     * The renderer this RenderTexture uses. A RenderTexture can only belong to one renderer at the moment if its webGL.
     *
     * @property renderer
     * @type CanvasRenderer|WebGLRenderer
     */
    this.renderer = renderer || PIXI.defaultRenderer;

    if (this.renderer.type === PIXI.WEBGL_RENDERER)
    {
        var gl = this.renderer.gl;
        this.baseTexture._dirty[gl.id] = false;

        this.textureBuffer = new PIXI.FilterTexture(gl, this.width, this.height, this.baseTexture.scaleMode);
        this.baseTexture._glTextures[gl.id] =  this.textureBuffer.texture;

        this.render = this.renderWebGL;
        this.projection = new PIXI.Point(this.width * 0.5, -this.height * 0.5);
    }
    else
    {
        this.render = this.renderCanvas;
        this.textureBuffer = new PIXI.CanvasBuffer(this.width * this.resolution, this.height * this.resolution);
        this.baseTexture.source = this.textureBuffer.canvas;
    }

    /**
     * @property valid
     * @type Boolean
     */
    this.valid = true;

    this.tempMatrix = new Phaser.Matrix();

    this._updateUvs();
};

PIXI.RenderTexture.prototype = Object.create(PIXI.Texture.prototype);
PIXI.RenderTexture.prototype.constructor = PIXI.RenderTexture;

/**
 * Resizes the RenderTexture.
 *
 * @method resize
 * @param width {Number} The width to resize to.
 * @param height {Number} The height to resize to.
 * @param updateBase {Boolean} Should the baseTexture.width and height values be resized as well?
 */
PIXI.RenderTexture.prototype.resize = function(width, height, updateBase)
{
    if (width === this.width &amp;&amp; height === this.height)return;

    this.valid = (width > 0 &amp;&amp; height > 0);

    this.width = width;
    this.height = height;
    this.frame.width = this.crop.width = width * this.resolution;
    this.frame.height = this.crop.height = height * this.resolution;

    if (updateBase)
    {
        this.baseTexture.width = this.width * this.resolution;
        this.baseTexture.height = this.height * this.resolution;
    }

    if (this.renderer.type === PIXI.WEBGL_RENDERER)
    {
        this.projection.x = this.width / 2;
        this.projection.y = -this.height / 2;
    }

    if(!this.valid)return;

    this.textureBuffer.resize(this.width, this.height);
};

/**
 * Clears the RenderTexture.
 *
 * @method clear
 */
PIXI.RenderTexture.prototype.clear = function()
{
    if (!this.valid)
    {
        return;
    }

    if (this.renderer.type === PIXI.WEBGL_RENDERER)
    {
        this.renderer.gl.bindFramebuffer(this.renderer.gl.FRAMEBUFFER, this.textureBuffer.frameBuffer);
    }

    this.textureBuffer.clear();
};

/**
 * This function will draw the display object to the texture.
 *
 * @method renderWebGL
 * @param displayObject {DisplayObject} The display object to render this texture on
 * @param [matrix] {Matrix} Optional matrix to apply to the display object before rendering.
 * @param [clear] {Boolean} If true the texture will be cleared before the displayObject is drawn
 * @private
 */
PIXI.RenderTexture.prototype.renderWebGL = function(displayObject, matrix, clear)
{
    if (!this.valid || displayObject.alpha === 0)
    {
        return;
    }
   
    //  Let's create a nice matrix to apply to our display object.
    //  Frame buffers come in upside down so we need to flip the matrix.
    var wt = displayObject.worldTransform;
    wt.identity();
    wt.translate(0, this.projection.y * 2);

    if (matrix)
    {
        wt.append(matrix);
    }

    wt.scale(1, -1);

    //  Time to update all the children of the displayObject with the new matrix.
    for (var i = 0; i &lt; displayObject.children.length; i++)
    {
        displayObject.children[i].updateTransform();
    }
    
    //  Time for the webGL fun stuff!
    var gl = this.renderer.gl;

    gl.viewport(0, 0, this.width * this.resolution, this.height * this.resolution);

    gl.bindFramebuffer(gl.FRAMEBUFFER, this.textureBuffer.frameBuffer );

    if (clear)
    {
        this.textureBuffer.clear();
    }

    this.renderer.spriteBatch.dirty = true;

    this.renderer.renderDisplayObject(displayObject, this.projection, this.textureBuffer.frameBuffer, matrix);

    this.renderer.spriteBatch.dirty = true;

};

/**
 * This function will draw the display object to the texture.
 *
 * @method renderCanvas
 * @param displayObject {DisplayObject} The display object to render this texture on
 * @param [matrix] {Matrix} Optional matrix to apply to the display object before rendering.
 * @param [clear] {Boolean} If true the texture will be cleared before the displayObject is drawn
 * @private
 */
PIXI.RenderTexture.prototype.renderCanvas = function(displayObject, matrix, clear)
{
    if (!this.valid || displayObject.alpha === 0)
    {
        return;
    }

    //  Let's create a nice matrix to apply to our display object.
    //  Frame buffers come in upside down so we need to flip the matrix.
    var wt = displayObject.worldTransform;
    wt.identity();

    if (matrix)
    {
        wt.append(matrix);
    }

    // Time to update all the children of the displayObject with the new matrix (what new matrix? there isn't one!)
    for (var i = 0; i &lt; displayObject.children.length; i++)
    {
        displayObject.children[i].updateTransform();
    }

    if (clear)
    {
        this.textureBuffer.clear();
    }

    var realResolution = this.renderer.resolution;

    this.renderer.resolution = this.resolution;

    this.renderer.renderDisplayObject(displayObject, this.textureBuffer.context, matrix);

    this.renderer.resolution = realResolution;
};

/**
 * Will return a HTML Image of the texture
 *
 * @method getImage
 * @return {Image}
 */
PIXI.RenderTexture.prototype.getImage = function()
{
    var image = new Image();
    image.src = this.getBase64();
    return image;
};

/**
 * Will return a base64 encoded string of this texture. It works by calling RenderTexture.getCanvas and then running toDataURL on that.
 *
 * @method getBase64
 * @return {String} A base64 encoded string of the texture.
 */
PIXI.RenderTexture.prototype.getBase64 = function()
{
    return this.getCanvas().toDataURL();
};

/**
 * Creates a Canvas element, renders this RenderTexture to it and then returns it.
 *
 * @method getCanvas
 * @return {HTMLCanvasElement} A Canvas element with the texture rendered on.
 */
PIXI.RenderTexture.prototype.getCanvas = function()
{
    if (this.renderer.type === PIXI.WEBGL_RENDERER)
    {
        var gl =  this.renderer.gl;
        var width = this.textureBuffer.width;
        var height = this.textureBuffer.height;

        var webGLPixels = new Uint8Array(4 * width * height);

        gl.bindFramebuffer(gl.FRAMEBUFFER, this.textureBuffer.frameBuffer);
        gl.readPixels(0, 0, width, height, gl.RGBA, gl.UNSIGNED_BYTE, webGLPixels);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);

        var tempCanvas = new PIXI.CanvasBuffer(width, height);
        var canvasData = tempCanvas.context.getImageData(0, 0, width, height);
        canvasData.data.set(webGLPixels);

        tempCanvas.context.putImageData(canvasData, 0, 0);

        return tempCanvas.canvas;
    }
    else
    {
        return this.textureBuffer.canvas;
    }
};
</code></pre>
        </article>
    </section>




</div>

<nav>
    <h2><a href="index.html">Home</a></h2><h3>Classes</h3><ul><li><a href="CanvasPool.html">CanvasPool</a></li><li><a href="EarCut.html">EarCut</a></li><li><a href="EventTarget.html">EventTarget</a></li><li><a href="PIXI.AbstractFilter.html">AbstractFilter</a></li><li><a href="PIXI.BaseTexture.html">BaseTexture</a></li><li><a href="PIXI.DisplayObject.html">DisplayObject</a></li><li><a href="PIXI.DisplayObjectContainer.html">DisplayObjectContainer</a></li><li><a href="PIXI.Event.html">Event</a></li><li><a href="PIXI.Graphics.html">Graphics</a></li><li><a href="PIXI.GraphicsData.html">GraphicsData</a></li><li><a href="PIXI.PIXI.html">PIXI</a></li><li><a href="PIXI.RenderTexture.html">RenderTexture</a></li><li><a href="PIXI.Rope.html">Rope</a></li><li><a href="PIXI.Sprite.html">Sprite</a></li><li><a href="PIXI.SpriteBatch.html">SpriteBatch</a></li><li><a href="PIXI.Strip.html">Strip</a></li><li><a href="PIXI.Texture.html">Texture</a></li><li><a href="PIXI.TilingSprite.html">TilingSprite</a></li><li><a href="PolyK.html">PolyK</a></li></ul><h3>Namespaces</h3><ul><li><a href="PIXI.html">PIXI</a></li></ul><h3>Global</h3><ul><li><a href="global.html#addChild">addChild</a></li><li><a href="global.html#addChildAt">addChildAt</a></li><li><a href="global.html">addTextureToCache</a></li><li><a href="global.html#arc">arc</a></li><li><a href="global.html#beginFill">beginFill</a></li><li><a href="global.html#bezierCurveTo">bezierCurveTo</a></li><li><a href="global.html#canUseNewCanvasBlendModes">canUseNewCanvasBlendModes</a></li><li><a href="global.html#clear">clear</a></li><li><a href="global.html">create</a></li><li><a href="global.html#destroy">destroy</a></li><li><a href="global.html#destroyCachedSprite">destroyCachedSprite</a></li><li><a href="global.html#dirty">dirty</a></li><li><a href="global.html#drawCircle">drawCircle</a></li><li><a href="global.html#drawEllipse">drawEllipse</a></li><li><a href="global.html#drawPolygon">drawPolygon</a></li><li><a href="global.html#drawRect">drawRect</a></li><li><a href="global.html#drawRoundedRect">drawRoundedRect</a></li><li><a href="global.html#drawShape">drawShape</a></li><li><a href="global.html#emit">emit</a></li><li><a href="global.html#endFill">endFill</a></li><li><a href="global.html#forceLoaded">forceLoaded</a></li><li><a href="global.html">fromCanvas</a></li><li><a href="global.html">fromFrame</a></li><li><a href="global.html">fromImage</a></li><li><a href="global.html#generateTexture">generateTexture</a></li><li><a href="global.html#generateTilingTexture">generateTilingTexture</a></li><li><a href="global.html#getBase64">getBase64</a></li><li><a href="global.html#getBounds">getBounds</a></li><li><a href="global.html#getCanvas">getCanvas</a></li><li><a href="global.html#getChildAt">getChildAt</a></li><li><a href="global.html#getChildIndex">getChildIndex</a></li><li><a href="global.html">getFirst</a></li><li><a href="global.html">getFree</a></li><li><a href="global.html#getImage">getImage</a></li><li><a href="global.html#getLocalBounds">getLocalBounds</a></li><li><a href="global.html#getNextPowerOfTwo">getNextPowerOfTwo</a></li><li><a href="global.html">getTotal</a></li><li><a href="global.html#hex2rgb">hex2rgb</a></li><li><a href="global.html#isPowerOfTwo">isPowerOfTwo</a></li><li><a href="global.html#lineStyle">lineStyle</a></li><li><a href="global.html#lineTo">lineTo</a></li><li><a href="global.html#listeners">listeners</a></li><li><a href="global.html#mixin">mixin</a></li><li><a href="global.html#moveTo">moveTo</a></li><li><a href="global.html#off">off</a></li><li><a href="global.html#on">on</a></li><li><a href="global.html#once">once</a></li><li><a href="global.html#preUpdate">preUpdate</a></li><li><a href="global.html#quadraticCurveTo">quadraticCurveTo</a></li><li><a href="global.html">remove</a></li><li><a href="global.html#removeAllListeners">removeAllListeners</a></li><li><a href="global.html">removeByCanvas</a></li><li><a href="global.html#removeChild">removeChild</a></li><li><a href="global.html#removeChildAt">removeChildAt</a></li><li><a href="global.html#removeChildren">removeChildren</a></li><li><a href="global.html#removeStageReference">removeStageReference</a></li><li><a href="global.html">removeTextureFromCache</a></li><li><a href="global.html#resize">resize</a></li><li><a href="global.html#rgb2hex">rgb2hex</a></li><li><a href="global.html#setChildIndex">setChildIndex</a></li><li><a href="global.html#setFrame">setFrame</a></li><li><a href="global.html#setStageReference">setStageReference</a></li><li><a href="global.html#setTexture">setTexture</a></li><li><a href="global.html#stopImmediatePropagation">stopImmediatePropagation</a></li><li><a href="global.html#stopPropagation">stopPropagation</a></li><li><a href="global.html#swapChildren">swapChildren</a></li><li><a href="global.html#syncUniforms">syncUniforms</a></li><li><a href="global.html#toGlobal">toGlobal</a></li><li><a href="global.html#toLocal">toLocal</a></li><li><a href="global.html#Triangulate">Triangulate</a></li><li><a href="global.html#unloadFromGPU">unloadFromGPU</a></li><li><a href="global.html#updateCache">updateCache</a></li><li><a href="global.html#updateLocalBounds">updateLocalBounds</a></li><li><a href="global.html#updateSourceImage">updateSourceImage</a></li></ul>
</nav>

<br class="clear">

<footer>
    Documentation generated by <a href="https://github.com/jsdoc3/jsdoc">JSDoc 3.4.0</a> on Thu Jun 09 2016 14:25:02 GMT-0400 (Eastern Daylight Time)
</footer>

<script> prettyPrint(); </script>
<script src="scripts/linenumber.js"> </script>
</body>
</html>
