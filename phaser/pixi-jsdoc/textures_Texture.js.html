<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JSDoc: Source: textures/Texture.js</title>

    <script src="scripts/prettify/prettify.js"> </script>
    <script src="scripts/prettify/lang-css.js"> </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="styles/prettify-tomorrow.css">
    <link type="text/css" rel="stylesheet" href="styles/jsdoc-default.css">
</head>

<body>

<div id="main">

    <h1 class="page-title">Source: textures/Texture.js</h1>

    



    
    <section>
        <article>
            <pre class="prettyprint source linenums"><code>/**
 * @author Mat Groves http://matgroves.com/ @Doormat23
 */

PIXI.TextureCache = {};
PIXI.FrameCache = {};

/**
 * TextureSilentFail is a boolean that defaults to `false`. 
 * If `true` then `PIXI.Texture.setFrame` will no longer throw an error if the texture dimensions are incorrect. 
 * Instead `Texture.valid` will be set to `false` (#1556)
 *
 * @type {boolean}
 */
PIXI.TextureSilentFail = false;

PIXI.TextureCacheIdGenerator = 0;

/**
 * A texture stores the information that represents an image or part of an image. It cannot be added
 * to the display list directly. Instead use it as the texture for a PIXI.Sprite. If no frame is provided then the whole image is used.
 *
 * @class Texture
 * @uses EventTarget
 * @constructor
 * @param baseTexture {BaseTexture} The base texture source to create the texture from
 * @param frame {Rectangle} The rectangle frame of the texture to show
 * @param [crop] {Rectangle} The area of original texture 
 * @param [trim] {Rectangle} Trimmed texture rectangle
 */
PIXI.Texture = function(baseTexture, frame, crop, trim)
{
    /**
     * Does this Texture have any frame data assigned to it?
     *
     * @property noFrame
     * @type Boolean
     */
    this.noFrame = false;

    if (!frame)
    {
        this.noFrame = true;
        frame = new PIXI.Rectangle(0,0,1,1);
    }

    if (baseTexture instanceof PIXI.Texture)
    {
        baseTexture = baseTexture.baseTexture;
    }

    /**
     * The base texture that this texture uses.
     *
     * @property baseTexture
     * @type BaseTexture
     */
    this.baseTexture = baseTexture;

    /**
     * The frame specifies the region of the base texture that this texture uses
     *
     * @property frame
     * @type Rectangle
     */
    this.frame = frame;

    /**
     * The texture trim data.
     *
     * @property trim
     * @type Rectangle
     */
    this.trim = trim;

    /**
     * This will let the renderer know if the texture is valid. If it's not then it cannot be rendered.
     *
     * @property valid
     * @type Boolean
     */
    this.valid = false;

    /**
     * Is this a tiling texture? As used by the likes of a TilingSprite.
     *
     * @property isTiling
     * @type Boolean
     */
    this.isTiling = false;

    /**
     * This will let a renderer know that a texture has been updated (used mainly for webGL uv updates)
     *
     * @property requiresUpdate
     * @type Boolean
     */
    this.requiresUpdate = false;

    /**
     * This will let a renderer know that a tinted parent has updated its texture.
     *
     * @property requiresReTint
     * @type Boolean
     */
    this.requiresReTint = false;

    /**
     * The WebGL UV data cache.
     *
     * @property _uvs
     * @type Object
     * @private
     */
    this._uvs = null;

    /**
     * The width of the Texture in pixels.
     *
     * @property width
     * @type Number
     */
    this.width = 0;

    /**
     * The height of the Texture in pixels.
     *
     * @property height
     * @type Number
     */
    this.height = 0;

    /**
     * This is the area of the BaseTexture image to actually copy to the Canvas / WebGL when rendering,
     * irrespective of the actual frame size or placement (which can be influenced by trimmed texture atlases)
     *
     * @property crop
     * @type Rectangle
     */
    this.crop = crop || new PIXI.Rectangle(0, 0, 1, 1);

    if (baseTexture.hasLoaded)
    {
        if (this.noFrame) frame = new PIXI.Rectangle(0, 0, baseTexture.width, baseTexture.height);
        this.setFrame(frame);
    }

};

PIXI.Texture.prototype.constructor = PIXI.Texture;

/**
 * Called when the base texture is loaded
 *
 * @method onBaseTextureLoaded
 * @private
 */
PIXI.Texture.prototype.onBaseTextureLoaded = function()
{
    var baseTexture = this.baseTexture;

    if (this.noFrame)
    {
        this.frame = new PIXI.Rectangle(0, 0, baseTexture.width, baseTexture.height);
    }

    this.setFrame(this.frame);
};

/**
 * Destroys this texture
 *
 * @method destroy
 * @param destroyBase {Boolean} Whether to destroy the base texture as well
 */
PIXI.Texture.prototype.destroy = function(destroyBase)
{
    if (destroyBase) this.baseTexture.destroy();

    this.valid = false;
};

/**
 * Specifies the region of the baseTexture that this texture will use.
 *
 * @method setFrame
 * @param frame {Rectangle} The frame of the texture to set it to
 */
PIXI.Texture.prototype.setFrame = function(frame)
{
    this.noFrame = false;

    this.frame = frame;
    this.width = frame.width;
    this.height = frame.height;

    this.crop.x = frame.x;
    this.crop.y = frame.y;
    this.crop.width = frame.width;
    this.crop.height = frame.height;

    if (!this.trim &amp;&amp; (frame.x + frame.width > this.baseTexture.width || frame.y + frame.height > this.baseTexture.height))
    {
        if (!PIXI.TextureSilentFail)
        {
            throw new Error('Texture Error: frame does not fit inside the base Texture dimensions ' + this);
        }

        this.valid = false;
        return;
    }

    this.valid = frame &amp;&amp; frame.width &amp;&amp; frame.height &amp;&amp; this.baseTexture.source &amp;&amp; this.baseTexture.hasLoaded;

    if (this.trim)
    {
        this.width = this.trim.width;
        this.height = this.trim.height;
        this.frame.width = this.trim.width;
        this.frame.height = this.trim.height;
    }
    
    if (this.valid) this._updateUvs();

};

/**
 * Updates the internal WebGL UV cache.
 *
 * @method _updateUvs
 * @private
 */
PIXI.Texture.prototype._updateUvs = function()
{
    if(!this._uvs)this._uvs = new PIXI.TextureUvs();

    var frame = this.crop;
    var tw = this.baseTexture.width;
    var th = this.baseTexture.height;
    
    this._uvs.x0 = frame.x / tw;
    this._uvs.y0 = frame.y / th;

    this._uvs.x1 = (frame.x + frame.width) / tw;
    this._uvs.y1 = frame.y / th;

    this._uvs.x2 = (frame.x + frame.width) / tw;
    this._uvs.y2 = (frame.y + frame.height) / th;

    this._uvs.x3 = frame.x / tw;
    this._uvs.y3 = (frame.y + frame.height) / th;
};

/**
 * Helper function that creates a Texture object from the given image url.
 * If the image is not in the texture cache it will be  created and loaded.
 *
 * @static
 * @method fromImage
 * @param imageUrl {String} The image url of the texture
 * @param crossorigin {Boolean} Whether requests should be treated as crossorigin
 * @param scaleMode {Number} See {{#crossLink "PIXI/scaleModes:property"}}PIXI.scaleModes{{/crossLink}} for possible values
 * @return {Texture}
 */
PIXI.Texture.fromImage = function(imageUrl, crossorigin, scaleMode)
{
    var texture = PIXI.TextureCache[imageUrl];

    if(!texture)
    {
        texture = new PIXI.Texture(PIXI.BaseTexture.fromImage(imageUrl, crossorigin, scaleMode));
        PIXI.TextureCache[imageUrl] = texture;
    }

    return texture;
};

/**
 * Helper function that returns a Texture objected based on the given frame id.
 * If the frame id is not in the texture cache an error will be thrown.
 *
 * @static
 * @method fromFrame
 * @param frameId {String} The frame id of the texture
 * @return {Texture}
 */
PIXI.Texture.fromFrame = function(frameId)
{
    var texture = PIXI.TextureCache[frameId];
    if(!texture) throw new Error('The frameId "' + frameId + '" does not exist in the texture cache ');
    return texture;
};

/**
 * Helper function that creates a new a Texture based on the given canvas element.
 *
 * @static
 * @method fromCanvas
 * @param canvas {Canvas} The canvas element source of the texture
 * @param scaleMode {Number} See {{#crossLink "PIXI/scaleModes:property"}}PIXI.scaleModes{{/crossLink}} for possible values
 * @return {Texture}
 */
PIXI.Texture.fromCanvas = function(canvas, scaleMode)
{
    var baseTexture = PIXI.BaseTexture.fromCanvas(canvas, scaleMode);

    return new PIXI.Texture(baseTexture);
};

/**
 * Adds a texture to the global PIXI.TextureCache. This cache is shared across the whole PIXI object.
 *
 * @static
 * @method addTextureToCache
 * @param texture {Texture} The Texture to add to the cache.
 * @param id {String} The id that the texture will be stored against.
 */
PIXI.Texture.addTextureToCache = function(texture, id)
{
    PIXI.TextureCache[id] = texture;
};

/**
 * Remove a texture from the global PIXI.TextureCache.
 *
 * @static
 * @method removeTextureFromCache
 * @param id {String} The id of the texture to be removed
 * @return {Texture} The texture that was removed
 */
PIXI.Texture.removeTextureFromCache = function(id)
{
    var texture = PIXI.TextureCache[id];
    delete PIXI.TextureCache[id];
    delete PIXI.BaseTextureCache[id];
    return texture;
};

PIXI.TextureUvs = function()
{
    this.x0 = 0;
    this.y0 = 0;

    this.x1 = 0;
    this.y1 = 0;

    this.x2 = 0;
    this.y2 = 0;

    this.x3 = 0;
    this.y3 = 0;
};
</code></pre>
        </article>
    </section>




</div>

<nav>
    <h2><a href="index.html">Home</a></h2><h3>Classes</h3><ul><li><a href="CanvasPool.html">CanvasPool</a></li><li><a href="EarCut.html">EarCut</a></li><li><a href="EventTarget.html">EventTarget</a></li><li><a href="PIXI.AbstractFilter.html">AbstractFilter</a></li><li><a href="PIXI.BaseTexture.html">BaseTexture</a></li><li><a href="PIXI.DisplayObject.html">DisplayObject</a></li><li><a href="PIXI.DisplayObjectContainer.html">DisplayObjectContainer</a></li><li><a href="PIXI.Event.html">Event</a></li><li><a href="PIXI.Graphics.html">Graphics</a></li><li><a href="PIXI.GraphicsData.html">GraphicsData</a></li><li><a href="PIXI.PIXI.html">PIXI</a></li><li><a href="PIXI.RenderTexture.html">RenderTexture</a></li><li><a href="PIXI.Rope.html">Rope</a></li><li><a href="PIXI.Sprite.html">Sprite</a></li><li><a href="PIXI.SpriteBatch.html">SpriteBatch</a></li><li><a href="PIXI.Strip.html">Strip</a></li><li><a href="PIXI.Texture.html">Texture</a></li><li><a href="PIXI.TilingSprite.html">TilingSprite</a></li><li><a href="PolyK.html">PolyK</a></li></ul><h3>Namespaces</h3><ul><li><a href="PIXI.html">PIXI</a></li></ul><h3>Global</h3><ul><li><a href="global.html#addChild">addChild</a></li><li><a href="global.html#addChildAt">addChildAt</a></li><li><a href="global.html">addTextureToCache</a></li><li><a href="global.html#arc">arc</a></li><li><a href="global.html#beginFill">beginFill</a></li><li><a href="global.html#bezierCurveTo">bezierCurveTo</a></li><li><a href="global.html#canUseNewCanvasBlendModes">canUseNewCanvasBlendModes</a></li><li><a href="global.html#clear">clear</a></li><li><a href="global.html">create</a></li><li><a href="global.html#destroy">destroy</a></li><li><a href="global.html#destroyCachedSprite">destroyCachedSprite</a></li><li><a href="global.html#dirty">dirty</a></li><li><a href="global.html#drawCircle">drawCircle</a></li><li><a href="global.html#drawEllipse">drawEllipse</a></li><li><a href="global.html#drawPolygon">drawPolygon</a></li><li><a href="global.html#drawRect">drawRect</a></li><li><a href="global.html#drawRoundedRect">drawRoundedRect</a></li><li><a href="global.html#drawShape">drawShape</a></li><li><a href="global.html#emit">emit</a></li><li><a href="global.html#endFill">endFill</a></li><li><a href="global.html#forceLoaded">forceLoaded</a></li><li><a href="global.html">fromCanvas</a></li><li><a href="global.html">fromFrame</a></li><li><a href="global.html">fromImage</a></li><li><a href="global.html#generateTexture">generateTexture</a></li><li><a href="global.html#generateTilingTexture">generateTilingTexture</a></li><li><a href="global.html#getBase64">getBase64</a></li><li><a href="global.html#getBounds">getBounds</a></li><li><a href="global.html#getCanvas">getCanvas</a></li><li><a href="global.html#getChildAt">getChildAt</a></li><li><a href="global.html#getChildIndex">getChildIndex</a></li><li><a href="global.html">getFirst</a></li><li><a href="global.html">getFree</a></li><li><a href="global.html#getImage">getImage</a></li><li><a href="global.html#getLocalBounds">getLocalBounds</a></li><li><a href="global.html#getNextPowerOfTwo">getNextPowerOfTwo</a></li><li><a href="global.html">getTotal</a></li><li><a href="global.html#hex2rgb">hex2rgb</a></li><li><a href="global.html#isPowerOfTwo">isPowerOfTwo</a></li><li><a href="global.html#lineStyle">lineStyle</a></li><li><a href="global.html#lineTo">lineTo</a></li><li><a href="global.html#listeners">listeners</a></li><li><a href="global.html#mixin">mixin</a></li><li><a href="global.html#moveTo">moveTo</a></li><li><a href="global.html#off">off</a></li><li><a href="global.html#on">on</a></li><li><a href="global.html#once">once</a></li><li><a href="global.html#preUpdate">preUpdate</a></li><li><a href="global.html#quadraticCurveTo">quadraticCurveTo</a></li><li><a href="global.html">remove</a></li><li><a href="global.html#removeAllListeners">removeAllListeners</a></li><li><a href="global.html">removeByCanvas</a></li><li><a href="global.html#removeChild">removeChild</a></li><li><a href="global.html#removeChildAt">removeChildAt</a></li><li><a href="global.html#removeChildren">removeChildren</a></li><li><a href="global.html#removeStageReference">removeStageReference</a></li><li><a href="global.html">removeTextureFromCache</a></li><li><a href="global.html#resize">resize</a></li><li><a href="global.html#rgb2hex">rgb2hex</a></li><li><a href="global.html#setChildIndex">setChildIndex</a></li><li><a href="global.html#setFrame">setFrame</a></li><li><a href="global.html#setStageReference">setStageReference</a></li><li><a href="global.html#setTexture">setTexture</a></li><li><a href="global.html#stopImmediatePropagation">stopImmediatePropagation</a></li><li><a href="global.html#stopPropagation">stopPropagation</a></li><li><a href="global.html#swapChildren">swapChildren</a></li><li><a href="global.html#syncUniforms">syncUniforms</a></li><li><a href="global.html#toGlobal">toGlobal</a></li><li><a href="global.html#toLocal">toLocal</a></li><li><a href="global.html#Triangulate">Triangulate</a></li><li><a href="global.html#unloadFromGPU">unloadFromGPU</a></li><li><a href="global.html#updateCache">updateCache</a></li><li><a href="global.html#updateLocalBounds">updateLocalBounds</a></li><li><a href="global.html#updateSourceImage">updateSourceImage</a></li></ul>
</nav>

<br class="clear">

<footer>
    Documentation generated by <a href="https://github.com/jsdoc3/jsdoc">JSDoc 3.4.0</a> on Thu Jun 09 2016 14:25:02 GMT-0400 (Eastern Daylight Time)
</footer>

<script> prettyPrint(); </script>
<script src="scripts/linenumber.js"> </script>
</body>
</html>
