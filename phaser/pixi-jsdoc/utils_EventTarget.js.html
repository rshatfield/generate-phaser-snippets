<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JSDoc: Source: utils/EventTarget.js</title>

    <script src="scripts/prettify/prettify.js"> </script>
    <script src="scripts/prettify/lang-css.js"> </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="styles/prettify-tomorrow.css">
    <link type="text/css" rel="stylesheet" href="styles/jsdoc-default.css">
</head>

<body>

<div id="main">

    <h1 class="page-title">Source: utils/EventTarget.js</h1>

    



    
    <section>
        <article>
            <pre class="prettyprint source linenums"><code>/**
 * @author Mat Groves http://matgroves.com/ @Doormat23
 * @author Chad Engler https://github.com/englercj @Rolnaaba
 */

/**
 * Originally based on https://github.com/mrdoob/eventtarget.js/ from mr Doob.
 * Currently takes inspiration from the nodejs EventEmitter, EventEmitter3, and smokesignals
 */

/**
 * Mixins event emitter functionality to a class
 *
 * @class EventTarget
 * @example
 *      function MyEmitter() {}
 *
 *      PIXI.EventTarget.mixin(MyEmitter.prototype);
 *
 *      var em = new MyEmitter();
 *      em.emit('eventName', 'some data', 'some more data', {}, null, ...);
 */
PIXI.EventTarget = {
    /**
     * Backward compat from when this used to be a function
     */
    call: function callCompat(obj) {
        if(obj) {
            obj = obj.prototype || obj;
            PIXI.EventTarget.mixin(obj);
        }
    },

    /**
     * Mixes in the properties of the EventTarget prototype onto another object
     *
     * @method mixin
     * @param object {Object} The obj to mix into
     */
    mixin: function mixin(obj) {
        /**
         * Return a list of assigned event listeners.
         *
         * @method listeners
         * @param eventName {String} The events that should be listed.
         * @return {Array} An array of listener functions
         */
        obj.listeners = function listeners(eventName) {
            this._listeners = this._listeners || {};

            return this._listeners[eventName] ? this._listeners[eventName].slice() : [];
        };

        /**
         * Emit an event to all registered event listeners.
         *
         * @method emit
         * @alias dispatchEvent
         * @param eventName {String} The name of the event.
         * @return {Boolean} Indication if we've emitted an event.
         */
        obj.emit = obj.dispatchEvent = function emit(eventName, data) {
            this._listeners = this._listeners || {};

            //backwards compat with old method ".emit({ type: 'something' })"
            if(typeof eventName === 'object') {
                data = eventName;
                eventName = eventName.type;
            }

            //ensure we are using a real pixi event
            if(!data || data.__isEventObject !== true) {
                data = new PIXI.Event(this, eventName, data);
            }

            //iterate the listeners
            if(this._listeners &amp;&amp; this._listeners[eventName]) {
                var listeners = this._listeners[eventName].slice(0),
                    length = listeners.length,
                    fn = listeners[0],
                    i;

                for(i = 0; i &lt; length; fn = listeners[++i]) {
                    //call the event listener
                    fn.call(this, data);

                    //if "stopImmediatePropagation" is called, stop calling sibling events
                    if(data.stoppedImmediate) {
                        return this;
                    }
                }

                //if "stopPropagation" is called then don't bubble the event
                if(data.stopped) {
                    return this;
                }
            }

            //bubble this event up the scene graph
            if(this.parent &amp;&amp; this.parent.emit) {
                this.parent.emit.call(this.parent, eventName, data);
            }

            return this;
        };

        /**
         * Register a new EventListener for the given event.
         *
         * @method on
         * @alias addEventListener
         * @param eventName {String} Name of the event.
         * @param callback {Functon} fn Callback function.
         */
        obj.on = obj.addEventListener = function on(eventName, fn) {
            this._listeners = this._listeners || {};

            (this._listeners[eventName] = this._listeners[eventName] || [])
                .push(fn);

            return this;
        };

        /**
         * Add an EventListener that's only called once.
         *
         * @method once
         * @param eventName {String} Name of the event.
         * @param callback {Function} Callback function.
         */
        obj.once = function once(eventName, fn) {
            this._listeners = this._listeners || {};

            var self = this;
            function onceHandlerWrapper() {
                fn.apply(self.off(eventName, onceHandlerWrapper), arguments);
            }
            onceHandlerWrapper._originalHandler = fn;

            return this.on(eventName, onceHandlerWrapper);
        };

        /**
         * Remove event listeners.
         *
         * @method off
         * @alias removeEventListener
         * @param eventName {String} The event we want to remove.
         * @param callback {Function} The listener that we need to find.
         */
        obj.off = obj.removeEventListener = function off(eventName, fn) {
            this._listeners = this._listeners || {};

            if(!this._listeners[eventName])
                return this;

            var list = this._listeners[eventName],
                i = fn ? list.length : 0;

            while(i-- > 0) {
                if(list[i] === fn || list[i]._originalHandler === fn) {
                    list.splice(i, 1);
                }
            }

            if(list.length === 0) {
                delete this._listeners[eventName];
            }

            return this;
        };

        /**
         * Remove all listeners or only the listeners for the specified event.
         *
         * @method removeAllListeners
         * @param eventName {String} The event you want to remove all listeners for.
         */
        obj.removeAllListeners = function removeAllListeners(eventName) {
            this._listeners = this._listeners || {};

            if(!this._listeners[eventName])
                return this;

            delete this._listeners[eventName];

            return this;
        };
    }
};

/**
 * Creates an homogenous object for tracking events so users can know what to expect.
 *
 * @class Event
 * @extends Object
 * @constructor
 * @param target {Object} The target object that the event is called on
 * @param name {String} The string name of the event that was triggered
 * @param data {Object} Arbitrary event data to pass along
 */
PIXI.Event = function(target, name, data) {
    //for duck typing in the ".on()" function
    this.__isEventObject = true;

    /**
     * Tracks the state of bubbling propagation. Do not
     * set this directly, instead use `event.stopPropagation()`
     *
     * @property stopped
     * @type Boolean
     * @private
     * @readOnly
     */
    this.stopped = false;

    /**
     * Tracks the state of sibling listener propagation. Do not
     * set this directly, instead use `event.stopImmediatePropagation()`
     *
     * @property stoppedImmediate
     * @type Boolean
     * @private
     * @readOnly
     */
    this.stoppedImmediate = false;

    /**
     * The original target the event triggered on.
     *
     * @property target
     * @type Object
     * @readOnly
     */
    this.target = target;

    /**
     * The string name of the event that this represents.
     *
     * @property type
     * @type String
     * @readOnly
     */
    this.type = name;

    /**
     * The data that was passed in with this event.
     *
     * @property data
     * @type Object
     * @readOnly
     */
    this.data = data;

    //backwards compat with older version of events
    this.content = data;

    /**
     * The timestamp when the event occurred.
     *
     * @property timeStamp
     * @type Number
     * @readOnly
     */
    this.timeStamp = Date.now();
};

/**
 * Stops the propagation of events up the scene graph (prevents bubbling).
 *
 * @method stopPropagation
 */
PIXI.Event.prototype.stopPropagation = function stopPropagation() {
    this.stopped = true;
};

/**
 * Stops the propagation of events to sibling listeners (no longer calls any listeners).
 *
 * @method stopImmediatePropagation
 */
PIXI.Event.prototype.stopImmediatePropagation = function stopImmediatePropagation() {
    this.stoppedImmediate = true;
};
</code></pre>
        </article>
    </section>




</div>

<nav>
    <h2><a href="index.html">Home</a></h2><h3>Classes</h3><ul><li><a href="CanvasPool.html">CanvasPool</a></li><li><a href="EarCut.html">EarCut</a></li><li><a href="EventTarget.html">EventTarget</a></li><li><a href="PIXI.AbstractFilter.html">AbstractFilter</a></li><li><a href="PIXI.BaseTexture.html">BaseTexture</a></li><li><a href="PIXI.DisplayObject.html">DisplayObject</a></li><li><a href="PIXI.DisplayObjectContainer.html">DisplayObjectContainer</a></li><li><a href="PIXI.Event.html">Event</a></li><li><a href="PIXI.Graphics.html">Graphics</a></li><li><a href="PIXI.GraphicsData.html">GraphicsData</a></li><li><a href="PIXI.PIXI.html">PIXI</a></li><li><a href="PIXI.RenderTexture.html">RenderTexture</a></li><li><a href="PIXI.Rope.html">Rope</a></li><li><a href="PIXI.Sprite.html">Sprite</a></li><li><a href="PIXI.SpriteBatch.html">SpriteBatch</a></li><li><a href="PIXI.Strip.html">Strip</a></li><li><a href="PIXI.Texture.html">Texture</a></li><li><a href="PIXI.TilingSprite.html">TilingSprite</a></li><li><a href="PolyK.html">PolyK</a></li></ul><h3>Namespaces</h3><ul><li><a href="PIXI.html">PIXI</a></li></ul><h3>Global</h3><ul><li><a href="global.html#addChild">addChild</a></li><li><a href="global.html#addChildAt">addChildAt</a></li><li><a href="global.html">addTextureToCache</a></li><li><a href="global.html#arc">arc</a></li><li><a href="global.html#beginFill">beginFill</a></li><li><a href="global.html#bezierCurveTo">bezierCurveTo</a></li><li><a href="global.html#canUseNewCanvasBlendModes">canUseNewCanvasBlendModes</a></li><li><a href="global.html#clear">clear</a></li><li><a href="global.html">create</a></li><li><a href="global.html#destroy">destroy</a></li><li><a href="global.html#destroyCachedSprite">destroyCachedSprite</a></li><li><a href="global.html#dirty">dirty</a></li><li><a href="global.html#drawCircle">drawCircle</a></li><li><a href="global.html#drawEllipse">drawEllipse</a></li><li><a href="global.html#drawPolygon">drawPolygon</a></li><li><a href="global.html#drawRect">drawRect</a></li><li><a href="global.html#drawRoundedRect">drawRoundedRect</a></li><li><a href="global.html#drawShape">drawShape</a></li><li><a href="global.html#emit">emit</a></li><li><a href="global.html#endFill">endFill</a></li><li><a href="global.html#forceLoaded">forceLoaded</a></li><li><a href="global.html">fromCanvas</a></li><li><a href="global.html">fromFrame</a></li><li><a href="global.html">fromImage</a></li><li><a href="global.html#generateTexture">generateTexture</a></li><li><a href="global.html#generateTilingTexture">generateTilingTexture</a></li><li><a href="global.html#getBase64">getBase64</a></li><li><a href="global.html#getBounds">getBounds</a></li><li><a href="global.html#getCanvas">getCanvas</a></li><li><a href="global.html#getChildAt">getChildAt</a></li><li><a href="global.html#getChildIndex">getChildIndex</a></li><li><a href="global.html">getFirst</a></li><li><a href="global.html">getFree</a></li><li><a href="global.html#getImage">getImage</a></li><li><a href="global.html#getLocalBounds">getLocalBounds</a></li><li><a href="global.html#getNextPowerOfTwo">getNextPowerOfTwo</a></li><li><a href="global.html">getTotal</a></li><li><a href="global.html#hex2rgb">hex2rgb</a></li><li><a href="global.html#isPowerOfTwo">isPowerOfTwo</a></li><li><a href="global.html#lineStyle">lineStyle</a></li><li><a href="global.html#lineTo">lineTo</a></li><li><a href="global.html#listeners">listeners</a></li><li><a href="global.html#mixin">mixin</a></li><li><a href="global.html#moveTo">moveTo</a></li><li><a href="global.html#off">off</a></li><li><a href="global.html#on">on</a></li><li><a href="global.html#once">once</a></li><li><a href="global.html#preUpdate">preUpdate</a></li><li><a href="global.html#quadraticCurveTo">quadraticCurveTo</a></li><li><a href="global.html">remove</a></li><li><a href="global.html#removeAllListeners">removeAllListeners</a></li><li><a href="global.html">removeByCanvas</a></li><li><a href="global.html#removeChild">removeChild</a></li><li><a href="global.html#removeChildAt">removeChildAt</a></li><li><a href="global.html#removeChildren">removeChildren</a></li><li><a href="global.html#removeStageReference">removeStageReference</a></li><li><a href="global.html">removeTextureFromCache</a></li><li><a href="global.html#resize">resize</a></li><li><a href="global.html#rgb2hex">rgb2hex</a></li><li><a href="global.html#setChildIndex">setChildIndex</a></li><li><a href="global.html#setFrame">setFrame</a></li><li><a href="global.html#setStageReference">setStageReference</a></li><li><a href="global.html#setTexture">setTexture</a></li><li><a href="global.html#stopImmediatePropagation">stopImmediatePropagation</a></li><li><a href="global.html#stopPropagation">stopPropagation</a></li><li><a href="global.html#swapChildren">swapChildren</a></li><li><a href="global.html#syncUniforms">syncUniforms</a></li><li><a href="global.html#toGlobal">toGlobal</a></li><li><a href="global.html#toLocal">toLocal</a></li><li><a href="global.html#Triangulate">Triangulate</a></li><li><a href="global.html#unloadFromGPU">unloadFromGPU</a></li><li><a href="global.html#updateCache">updateCache</a></li><li><a href="global.html#updateLocalBounds">updateLocalBounds</a></li><li><a href="global.html#updateSourceImage">updateSourceImage</a></li></ul>
</nav>

<br class="clear">

<footer>
    Documentation generated by <a href="https://github.com/jsdoc3/jsdoc">JSDoc 3.4.0</a> on Thu Jun 09 2016 14:25:02 GMT-0400 (Eastern Daylight Time)
</footer>

<script> prettyPrint(); </script>
<script src="scripts/linenumber.js"> </script>
</body>
</html>
