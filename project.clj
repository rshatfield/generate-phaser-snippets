(defproject phaserSnippets "0.1.0-SNAPSHOT"
    :dependencies [[org.clojure/clojure "1.7.0"]
                   [org.clojure/clojurescript "1.7.48"]]
    :plugins [[lein-cljsbuild "1.1.0"]]
    :cljsbuild
    {:builds
        [{:source-paths ["src"],
          :compiler {
            :output-to "phaser-snippets.js"
            :target :nodejs
            :optimizations :simple
            :pretty-print false
           }
        }]
    })